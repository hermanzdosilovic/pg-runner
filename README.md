# pg-runner 

Pg-runner is a PostgreSQL runner for Edgar APAS.
This project is needed if you want to run SQL questions in Edgar.

Setup:

 * Install latest Node.js
 * `git clone git@gitlab.com:edgar-group/pg-runner.git`
 * `npm install`
 * Create files (by copying and adjusting `config/development-config-TEMPLATE.js`):
   * `config/development-config.js`
   * `config/production-config.js`
 * Create 5 test databases using `db/db-schema/studadmin.sql` or some other database you want to test students on
 * Run pg-runner with: `npm start`
 