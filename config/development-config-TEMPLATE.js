module.exports = {
    PUBLIC: 1,
    TIMEOUT: 15000,
    MAX_INSTANCES: 5,
    URL: '/ssrunner',
    DB: 'streamservice0',
    poolConfig: {
        user: 'pgrunner',
        database: 'streamservice0',
        password: '***',
        port: 5432,
        host: '127.0.0.1',
        max: 100,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 2000,
    }
};