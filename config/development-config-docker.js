module.exports = {
    PUBLIC: 1,
    TIMEOUT: 15000,
    MAX_INSTANCES: 1,
    URL: '/ssrunner',
    DB: 'masterexam01',
    poolConfig: {
        user: 'pgrunner',
        database: 'masterexam01',
        password: 'localhost127',
        port: 5432,
        host: 'pgrunnerdb',
        max: 100,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 2000,
    }
};