#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
  CREATE ROLE pgrunner WITH LOGIN PASSWORD 'localhost127' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';
  CREATE DATABASE masterexam01 WITH OWNER = pgrunner ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'hr_HR.UTF-8' LC_CTYPE = 'hr_HR.UTF-8' CONNECTION
  LIMIT = -1 TEMPLATE template0;
EOSQL

pg_restore -U pgrunner --dbname=masterexam01 --verbose /docker-entrypoint-initdb.d/masterexam01.dmp
