'use strict';
// server.js
var http = require('http');
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var moment = require('moment');
var osInfo = require('./common/osInfo');
var winston = require('winston');
const cluster = require('cluster');
let winstonFormat;
if (process.env.NODE_ENV === 'development') {
    winston.level = 'debug';
    winstonFormat = winston.format.printf(({ level, message, timestamp, script }) => {
        return ` ${level} ${moment(timestamp).format('LTS')} [${script.replace(global.appRoot, '')}]: ${message}`;
    });
} else {
    winston.level = 'info';
    winstonFormat = winston.format.json();
}
winston.configure({    
    format: winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
        winstonFormat
    ),
    defaultMeta: {
        username: osInfo.getUserName(),
        hostname: osInfo.getHostName(),
        script: osInfo.getScriptName(),
        appInstance: osInfo.appInstance(),
        source: 'pg-runner'
    },
    transports: [
        new winston.transports.Console(),
    ]
});
const pg = require('pg');
const parseInterval = require('postgres-interval');
const config = require('./config/config');

const instance = ((parseInt(process.env.pm_id) || 0) % config.MAX_INSTANCES) + 1;

if (config.poolConfig.database.endsWith("0")) {
    config.poolConfig.database += instance;
    config.DB += instance;
}

const pool = new pg.Pool(config.poolConfig);
pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err);
    process.exit(-1);
});
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


var port = process.env.PORT || 10080; // set our port



// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

//Date.prototype.toJSON = function() { return this.toLocaleString('de-DE'); };

//var pg = require('pg');
var types = pg.types;
types.setTypeParser(1114, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1115, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1182, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1185, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1082, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1184, function (stringValue) {
    return stringValue;
});
types.setTypeParser(1186, function (stringValue) {
    return parseInterval(stringValue).toPostgres();
});


// Potentially expose Prometheus metrics:
const EXPOSE_METRICS = !!process.env.METRICS;
let metrics;
if (EXPOSE_METRICS) {
    winston.info('**********************************************************');
    winston.info('********************  Exposing metrics. ******************');
    winston.info('**********************************************************');
    var expressResponseSize = require('express-response-size');
    const prometheus = require('prom-client');
    const collectDefaultMetrics = prometheus.collectDefaultMetrics;
    const prefix = 'pg_runner_';
    collectDefaultMetrics({
        prefix
    });
    metrics = {
        requestCounter: new prometheus.Counter({
            name: prefix + 'sql_request_count',
            help: 'The number of requests for SQL code execution.'
        }),
        outcomeCounter: new prometheus.Counter({
            name: prefix + 'sql_outcome_count',
            help: 'The number of outcomes for SQL code execution',
            labelNames: ['outcome_status']
        }),
        httpRequestDuration: new prometheus.Histogram({
            name: prefix + 'http_request_duration_seconds',
            help: 'Request duration in seconds',
            buckets: [0.1, 0.5, 1, 2, 5, 15, 50, 100, 500],
        }),
        httpResponseSize: new prometheus.Histogram({
            name: prefix + 'http_response_size_bytes',
            help: 'Response size in bytes',
            buckets: [1000, 5000, 10000, 100000, 200000, 500000],
        }),
    }
    app.use(expressResponseSize((req, res, size) => {
        metrics.httpResponseSize.observe(parseInt(size));
    }));
    
    if (process.env.pm_id !== undefined) {

        winston.info('Cluster mode, metrics will be aggregated. ' + cluster.isMaster + ' ' + cluster.isWorker);
        const pm2mes = require('pm2-messages');
        pm2mes.onMessage('get_prom_register', (packet) => prometheus.register.getMetricsAsJSON());

        router.get('/metrics', async (req, res) => {
            try {
                await pm2mes.connect();
                const metricsArr = await pm2mes.getMessages('get_prom_register');
                await pm2mes.disconnect();
                const metrics = await prometheus.AggregatorRegistry.aggregate(metricsArr).metrics();
                res.end(metrics);
            } catch (error) {
                console.error(error)
                winston.error(error);
                res.status(500).end(error.message);
            }
        });

    } else {

        router.get('/metrics', (req, res) => {
            res.end(prometheus.register.metrics())
        });
    }

}

router.get('/ping', (req, res) => {
    res.end("pong!");
});

config.TIMEOUT = config.TIMEOUT || 2500;

winston.info(`Mounting: ${config.URL}`);
router.post(`${config.URL}`, function (req, res) {
    // TODO remove stop words!
    //  This is unsecure by design
    let end;
    EXPOSE_METRICS && metrics.requestCounter.inc();
    EXPOSE_METRICS && (end = metrics.httpRequestDuration.startTimer());
    console.log(req.body);
    
    var sql = req.body.sql;
    var dbSchema = req.body.db_schema;
    var timeout = req.body.timeout || config.TIMEOUT;
    if (!sql) {
        res.json({
            success: false,
            data: {},
            db: config.DB,
            error: {
                message: 'Query not defined, nothing to execute.'
            }
        });
    } else {
        let checkSQL = sql.toLowerCase().trim().split(/\s+/);
        let ok = true;
        if (checkSQL && checkSQL.length) {
            for (let word of checkSQL) {
                if (
                    ['begin',
                        //'beginwork', 
                        //'begintransaction',
                        //'startwork',
                        //'starttransaction',
                        'start',
                        //'commitwork',
                        //'committransaction',
                        'transaction',
                        'commit',
                        'rollback'
                    ].includes(word)
                ) {
                    ok = false;
                    break;
                }
            }
        }
        if (!ok) {
            winston.error('Hack attempted: ' + sql);
            sql = `SELECT 'Forbidden use of tranasactions. Incident logged and reported!';`;
        } else {
            sql = `SET SESSION STATEMENT_TIMEOUT TO ${timeout};\nBEGIN WORK;\nSET search_path = ${dbSchema ? dbSchema + ",": ""} public;\n\n${sql}\n;\nROLLBACK WORK;`;
        }
        //sql = 'BEGIN WORK;\n\n' + sql + '\n;\nROLLBACK WORK;';
        winston.info('Executing on instance ' + config.DB + ':\n-----------------------\n' + sql + '\n----------------------\n');
        //db.query({ text: sql, rowMode: 'array'})
        let LABEL = 'Exec time: ' + config.DB + ' ' + process.hrtime().join(':');
        console.time(LABEL);
        pool.connect( function (err, client, done) {

            if (err) {
                EXPOSE_METRICS && end();
                EXPOSE_METRICS && metrics.outcomeCounter.labels('ERROR').inc();
                winston.error(err);
                throw err; // this will kill the app, that's OK
            }
            client.query({
                text: sql,
                rowMode: 'array'
            }, function (err, data) {
                if (err) {
                    EXPOSE_METRICS && end(); // TODO - branch out below
                    console.timeEnd(LABEL);
                    client.query('ROLLBACK', function (rbErr) {
                        //if there was a problem rolling back the query
                        //something is seriously messed up.  Return the error
                        //to the done function to close & remove this client from
                        //the pool.  If you leave a client in the pool with an unaborted
                        //transaction weird, hard to diagnose problems might happen.
                        if (rbErr) {
                            EXPOSE_METRICS && metrics.outcomeCounter.labels('SERIOUS ROLLBACK ERROR').inc();
                            winston.error('--> SERIOUS ROLLBACK error, message: ' + rbErr);
                            res.json({
                                success: false,
                                data: {},
                                db: config.DB,
                                error: {
                                    message: '!! SERIOUS ROLLBACK error, message:' + rbErr.message
                                }
                            });
                            return done(rbErr);
                        } else {
                            winston.info('Query error, message: ' + err.message + '\nSQL:\n' + sql);
                            if (err.message.indexOf('statement timeout') > 0) {
                                EXPOSE_METRICS && metrics.outcomeCounter.labels('SQL sttmnt timeout').inc();
                                res.json({
                                    success: false,
                                    data: {},
                                    db: config.DB,
                                    warning: 'Statement canceled due to timeout, are you performing cross joins? Incident logged.',
                                    error: {
                                        message: err.message,
                                        position: err.position
                                    }
                                });
                            } else {
                                EXPOSE_METRICS && metrics.outcomeCounter.labels('SQL sttmnt error').inc();
                                res.json({
                                    success: false,
                                    data: {},
                                    db: config.DB,
                                    error: {
                                        message: err.message,
                                        position: err.position
                                    }
                                });
                            }
                        }
                        //client.end();
                    });
                } else {
                    //as long as we do not call the `done` callback we can do
                    //whatever we want...the client is ours until we call `done`
                    //on the flip side, if you do call `done` before either COMMIT or ROLLBACK
                    //what you are doing is returning a client back to the pool while it
                    //is in the middle of a transaction.
                    //Returning a client while its in the middle of a transaction
                    //will lead to weird & hard to diagnose errors.
                    
                    // pg changed the format of data, now it is an aray, so I'll grab the last element which is not the rollback
                    //  should always be next to last
                    for (let i =  data.length - 1; i >= 0; --i) {
                        if (data[i].command.toUpperCase() !== 'ROLLBACK') {
                            if (i != data.length - 2) {
                                console.error("WARNING: missing ROLLBACK command !?", i, data.length)
                            }
                            data = data[i];
                            break;
                        }
                    }
                    if (data.rows && data.rows.length) {
                        data.rowCount = data.rows.length;
                    } else {
                        data.rowCount = 0;
                    }
                    var warning;
                    if (data.rowCount > 1000) {
                        winston.info('*********** OVER 1000 rows, truncating to 1000!');
                        data.rows.splice(1000); // max 1k rows
                        warning = 'Resultset too large(' + data.rowCount + '), truncated to 1000 rows. Be careful.';
                    }
                    var edgarData = [];
                    for (var i = 0; i < data.rowCount && i < 1000; ++i) {
                        var item = {};
                        for (var j = 0; j < data.fields.length; ++j) {
                            item['C' + j] = data.rows[i][j];
                        }
                        edgarData.push(item);
                    }
                    data.rows = edgarData;
                    winston.info('Success, returning rowcount: ' + data.rowCount);
                    console.timeEnd(LABEL);
                    EXPOSE_METRICS && metrics.outcomeCounter.labels('success').inc();
                    EXPOSE_METRICS && end();
                    if (warning) {
                        res.json({
                            success: true,
                            warning: warning,
                            data: data,
                            db: config.DB
                        });
                    } else {
                        res.json({
                            success: true,
                            data: data,
                            db: config.DB
                        });
                    }
                }
                return done();
            });
        });
    }

});



// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
//app.use('/api', router);
app.use(router);

app.use(function(req, res) {
    winston.error("User trying: " + req.url + " should be " + config.URL);
    res.json({
        success: false,
        data: {},
        db: config.DB,
        error: {
            message: 'Wrong endpoint (' + req.url + '), see config file...'
        }
    });
});

// START THE SERVER
// =============================================================================

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

if (config.PUBLIC) {
    server.listen(port, '0.0.0.0'); // , '0.0.0.0'
} else {
    server.listen(port, 'localhost');
}

server.on('error', onError);
server.on('listening', onListening);

function onError(error) {
    winston.error('Global onerror handler: ' + error);
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            winston.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            winston.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    // var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    winston.info(' Running the ps sql runner on: ' + JSON.stringify(addr));
    winston.info(' Config is: ' + JSON.stringify(config));
    winston.info(' Instance is ' + instance);
}
