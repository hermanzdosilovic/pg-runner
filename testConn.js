'use strict';
var moment = require('moment');
var osInfo = require('./common/osInfo');
var winston = require('winston');
let winstonFormat;
if (process.env.NODE_ENV === 'development') {
    winston.level = 'debug';
    winstonFormat = winston.format.printf(({ level, message, timestamp, script }) => {
        return ` ${level} ${moment(timestamp).format('LTS')} [${script.replace(global.appRoot, '')}]: ${message}`;
    });
} else {
    winston.level = 'info';
    winstonFormat = winston.format.json();
}
winston.configure({    
    format: winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
        winstonFormat
    ),
    defaultMeta: {
        username: osInfo.getUserName(),
        hostname: osInfo.getHostName(),
        script: osInfo.getScriptName(),
        appInstance: osInfo.appInstance(),
        source: 'pg-runner'
    },
    transports: [
        new winston.transports.Console(),
    ]
});
var pg = require('pg');
var config = require('./config/config');
// configure app to use bodyParser()
// this will let us get the data from a POST

var instance = ((parseInt(process.env.pm_id) || 0) % 5) + 1;
//instance = 6; // dok ne doznam kako se baze razdese :(
if (config.PG_CONNECTION_STRING.endsWith("0") && config.DB.endsWith("0")) {
    config.PG_CONNECTION_STRING += instance;
    config.DB += instance;
}

config.TIMEOUT = config.TIMEOUT || 2500;
console.log("connecting to:", config.PG_CONNECTION_STRING);
pg.connect(config.PG_CONNECTION_STRING, function (err, client, done) {
    console.log("connected");
    if (err) {
        console.log("Error occured!!");
        console.log(err);
        throw err; // this will kill the app, that's OK
    }
    client.query({
        text: 'SELECT * FROM pg_tables;',
        rowMode: 'array'
    }, function (err, data) {
        if (err) {
            console.log("Error occured!!");
            console.log(err);
            throw err;
        } else {
            if (data.rows && data.rows.length) {
                data.rowCount = data.rows.length;
            } else {
                data.rowCount = 0;
            }
            console.log('Success, returning rowcount: ' + data.rowCount);
        }
        return done();
    });
});